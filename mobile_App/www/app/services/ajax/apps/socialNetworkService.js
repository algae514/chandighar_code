'use strict';
/**
 * Components that are must 
 */
angular.module('myApp.socialNetworkService', ['ngRoute']).service('socialNetworkService', ['$http', 'statsUpdateService', 'appAdminService','$state', function ($http, statsUpdateService, appAdminService,$state) {

        var cudAppurl = centralServer + '/cudApp'
        var cudSubpageIntrourl = centralServer + '/cudSubPageIntro'
        var cudSubpageFullurl = centralServer + '/cudSubPageFull'
        var getFullDetails = centralServer + '/getFullDetails'
        var bookmarkAppURL = centralServer + '/bookmarkApp'
        var createUserURL = centralServer + '/createUser'
        var sendNotificationURL = centralServer + '/sendNotification'


        this.register = function (user) {
            appAdminService.register(user);
        }


        this.loginGP = function () {

            // window.plugins.spinnerDialog.show("Logging in", "Opening doors to fun :) ", true);

            var gPData = gPlusLogin(function () {
                // $location.path("/home");
                $state.go("app.home");
                document.getElementsByTagName("ion-header-bar").style.display="block";
            });
        }




        function gPlusLogin(someFunOnSucess) {
            var gplusData = null;

            window.plugins.googleplus.login(
                    {
                        'scopes': 'profile https://www.googleapis.com/auth/contacts.readonly', // optional space-separated list of scopes, the default is sufficient for login and basic profile info
                        'offline': true // optional, used for Android only - if set to true the plugin will also return the OAuth access token ('oauthToken' param), that can be used to sign in to some third party services that don't accept a Cross-client identity token (ex. Firebase)
                    },
            function (obj) {
                gplusData = obj;

                //  alert(" gplusData : " + JSON.stringify(gplusData));
                // window.location = "#/home"
                // window.plugins.spinnerDialog.hide();
                // $ionicLoading.hide();
                // document.getElementsByTagName("ion-header-bar").style.display="block";

                $state.go("app.home");
                statsUpdateService.log(gplusData);
                appAdminService.register(gplusData);
            },
                    function (msg) {
                        alert("error: " + msg);
                        statsUpdateService.log("gplus login error callback message"+gplusData+" error : "+msg);
                    }
            );
            return gplusData;
        }



        function getContacts(userDAta) {
            var token = userDAta.oauthToken;

            $.getJSON('https://www.google.com/m8/feeds/contacts/default/full/?access_token=' +
                    token + "&alt=json&callback=?", function (result) {
                        console.log(JSON.stringify(result));
                    });

            /*
             $.getJSON('https://www.google.com/m8/feeds/contacts/default/full/?access_token=' +
             authResult.access_token + "&alt=json&callback=?", function (result) {
             console.log(JSON.stringify(result));
             });
             */

        }


    }]);
