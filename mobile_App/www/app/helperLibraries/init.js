function initMethodsList() {
    enableKeyboardFunction();

    console.log(" userINTROOID " + userINTROOID);

    // alert("taking push notifications here !!!")

    if (userINTROOID == null || userINTROOID == undefined || userINTROOID == "") {
        console.log("Logging in as guest user.");
        // userINTROOID = "guestUser";

        // alert("taking push notifications here !!!")
        pushNotificationInitiate();

    } else {

        console.log("Logging in as authenticated user .");
        pushNotificationInitiate();

    }

}


function initPushNotificatins($cordovaPushV5){



        // initialize push notifications here 
        // command : cordova plugin add phonegap-plugin-push --variable SENDER_ID="601672273692"
        var options = {
            android: {
                senderID: "601672273692"
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {}
        };


        // alert(" about to init push notes, doc is ready .")  
        try {


            // initialize
            $cordovaPushV5.initialize(options).then(function() {


                // start listening for new notifications
                $cordovaPushV5.onNotification();

                // start listening for errors
                $cordovaPushV5.onError();


                // register to get registrationId
                $cordovaPushV5.register().then(function(registrationId) {
                    // save `registrationId` somewhere;



                    regId_M = registrationId;
                    // alert("  registrationId "+registrationId)

                })



            });


            // triggered every time notification received
            $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
                // data.message,
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData
                alert("New Issue created !");
            });


            // alert("  $cordovaPushV5:errorOcurred GETTING READY  ")
            // triggered every time error occurs
            $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e) {
                // e.message
                // alert("  $cordovaPushV5:errorOcurred  "+JSON.stringify(e)+"\n msg : "+e.message+"\n : "+JSON.stringify(event))

            });


        } catch (err) {
            alert(" eror "+JSON.stringify(err)+"err"+err)
        }

alert(" ppush notifications init completed method ... ")
}


// cordova plugin add phonegap-plugin-push --variable SENDER_ID="601672273692"


function pushNotificationInitiate() {

    // alert(" device is ready now !!!");

    var push = PushNotification.init({
        android: {
            senderID: "601672273692"
        },
        ios: {
            alert: "true",
            badge: true,
            sound: 'false'
        },
        windows: {}
    });



    push.on('registration', function(data) {
        // alert("about to start registration !!! globalPageIntroOid "+globalPageIntroOid);
        var regId = data.registrationId;
        var urlIbn = centralServer + "/regUserForNtfnsPhysicalApp/" + escape(userINTROOID) + "/" + escape(regId)+"/"+escape(globalPageIntroOid);
        // alert("urlIbn "+urlIbn);
        saveData("registrationId", data.registrationId);
        // alert(" recieved registration id "+data.registrationId+"userINTROOID "+userINTROOID);

        var xhttp = new XMLHttpRequest();


        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                // document.getElementById("demo").innerHTML = xhttp.responseText;
                // alert("xhttp.responseText"+xhttp.responseText);
            }
        };


        xhttp.open("GET", urlIbn, true);
        // alert(" open data to server ");

        xhttp.send();
        // alert(" sent ");



    });



    push.on('notification', function(data) {
        // data.message,
        // data.title,
        // data.count,
        // data.sound,
        // data.image,
        // data.additionalData
        console.log(" gotPUSH PUSH PUSH  notification " + JSON.stringify(data))
    });

}





function enableKeyboardFunction() {
    // alert(" enableKeyboardFunction");

    // keyboard start
    window.addEventListener('native.keyboardshow', function(e) {
        var deviceHeight = window.innerHeight;
        var keyboardHeight = e.keyboardHeight;
        var deviceHeightAdjusted = deviceHeight - keyboardHeight; //device height adjusted
        deviceHeightAdjusted = deviceHeightAdjusted < 0 ? (deviceHeightAdjusted * -1) : deviceHeightAdjusted; //only positive number
        document.getElementById('page').style.height = deviceHeightAdjusted + 'px'; //set page height
        document.getElementById('page').setAttribute('keyBoardHeight', keyboardHeight); //save keyboard height
    });


    // keyboard closed
    window.addEventListener('native.keyboardhide', function(e) {
        setTimeout(function() {
            document.getElementById('page').style.height = 100 + '%'; //device  100% height
        }, 100);
    });


}
