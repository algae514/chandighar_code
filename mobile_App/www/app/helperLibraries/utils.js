var statCodes = {};

statCodes.LOGIN_START = "0001";
statCodes.LOGIN_SUCCESS = "0002";
statCodes.LOGIN_FAIL = "0003";
statCodes.REGISTRATION = "0004";



function b64toBlob(b64Data, contentType, sliceSize) {
    // console.log(" b64Data "+b64Data)

    if (b64Data == undefined) {
        return null;
    }

    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    // console.log(" before : returning blob ! byteArrays size : " + byteArrays.length);

    try {
        var blob = new Blob(byteArrays, {
            type: contentType
        });


    } catch (e) {


        // TypeError old chrome and FF
        window.BlobBuilder = window.BlobBuilder ||
            window.WebKitBlobBuilder ||
            window.MozBlobBuilder ||
            window.MSBlobBuilder;

        if (e.name == 'TypeError' && window.BlobBuilder) {
            try {
                var bb = new BlobBuilder();
                bb.append(byteArrays);
                var blob = bb.getBlob(contentType);
                return blob;

            } catch (newE) {
                console.log(" internal error : " + newE)
            }

        } else if (e.name == "InvalidStateError") {
            // InvalidStateError (tested on FF13 WinXP)
            var blob = new Blob(byteArrays, { type: "image/jpeg" });
            return blob;


        } else {

            // We're screwed, blob constructor unsupported entirely   
            console.log("  screwed ! :) ")

        }

    }





    return blob;
}

function blobToFile(theBlob, fileName) {
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
}

function getFile(file_src) {
    i = 0;
    console.log(" getting file " + "image" + i);
    // // var file = document.getElementById("image" + i).files[0];
    // // var file = document.getElementById("imageFinalImage" + i).src;
    // var file_src = document.getElementById("imageFinalImage" + i).src;

    var blob = b64toBlob(file_src.split(",")[1], '');
    if (blob == null) {
        return null;
    }
    console.log(" blob recieved : ")

    var myURL = window.URL || window.webkitURL
    var blobUrl = myURL.createObjectURL(blob);


    var file = blobToFile(blob, "image" + i);
    // alert(" file input  " + (file))

    if (file == undefined) {
        return null;
    }

    console.log("file  size " + (file.length / 1024));
    // cropFImage(file);

    if ((file.length / 1024) > 400) {
        alert("Maximum file size must be less than 400 kb");
        return null
    }


    return file;
}






// later take into separate commonm functionality librarty start 
function saveData(key, value) {
    key = "GFAD_DATA_PRE" + key;
    localStorage.setItem(key, value);
    //    console.log(" savng data key:" + key + "# value:" + value);
}

function getSavedData(key) {
    key = "GFAD_DATA_PRE" + key;
    return localStorage.getItem(key);
}

function isAlphaNumeric(toBeChecked) {
    var username = toBeChecked;
    var pattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/); //unacceptable chars
    if (pattern.test(username)) {

        return false;
    }
    return true; //good user input
}

function cacheJson(key, value) {
    var key = "GFAD_AJAX_DATA_PRE" + key;
    localStorage.setItem(key, value);
}


function getCachedJson(key) {
    var key = "GFAD_AJAX_DATA_PRE" + key;
    return localStorage.getItem(key);
}

function isDataDiffFromCache(key, valueRetrieved) {
    var key = "GFAD_AJAX_DATA_PRE" + key;
    var cachedJson = getCachedJson(key)
    if (cachedJson === valueRetrieved) {
        return false;
    } else {
        cacheJson(key, valueRetrieved);
        return true;
    }
}


function cacheJson(key, value) {
    var key = "GFAD_AJAX_DATA_PRE" + key;
    key = key.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    console.log(" cacheJson(key,value)  " + key + "  value len " + JSON.stringify(value).length)
    localStorage.setItem(key, JSON.stringify(value));
}


function getCachedJson(key) {
    var key = "GFAD_AJAX_DATA_PRE" + key;
    key = key.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    console.log(" getCachedJson(key) " + key)
    return JSON.parse(localStorage.getItem(key));
}

function isDataDiffFromCache(key, valueRetrieved) {

    var keyWithPre = "GFAD_AJAX_DATA_PRE" + key;
    key = key.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    console.log(" cache check : " + key)

    var cachedJson = getCachedJson(key)
    console.log("cachedJson: " + cachedJson)
    if (JSON.stringify(cachedJson) === JSON.stringify(valueRetrieved)) {
        return false;
    } else {
        cacheJson(key, valueRetrieved);
        return true;
    }
}
