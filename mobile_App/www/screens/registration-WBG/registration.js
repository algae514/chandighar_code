'use strict';

angular.module('myApp.registration-WBG', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/registration-WBG/:pageOid/:appIntroOid', {
        templateUrl: 'screens/registration-WBG/registration.html',
        controller: 'registrationCtrl'
    });
}])

.controller('registrationCtrl', [
    '$scope',
    '$location',
    '$state',
    '$stateParams',
    '$cordovaCamera',
    '$cordovaImagePicker',
    '$http',
    '$ionicLoading',
    '$ionicPopup',
    '$timeout',

    'socialNetworkService',
    'statsUpdateService',
    'appAdminService',
    function($scope, $location, $state, $stateParams, $cordovaCamera, $cordovaImagePicker, $http, $ionicLoading, $ionicPopup, $timeout, socialNetworkService, statsUpdateService, appAdminService) {

        $scope.centralServer = centralServer;
        $scope.centralServer1 = centralServer1;
        $scope.submitDisabled = false;
        $scope.isStatusInQuestion = false;

        var pageIntroOid = $stateParams.pageOid;
        var reponseOid = $stateParams.appIntroOid;
        var pageDetailsId = "";
        console.log(" inputs retrieved :  pageIntroOid " + pageIntroOid + " reponseOid " + reponseOid)

        var appPage = "RegistrationPageResponseSubmit page";
        var imageUploadURL = centralServer + '/uploadImage';
        var alertPopupV;

        $scope.imgURI = "";
        loadRegistrationQuestions();

        $scope.submitResponses = function() {
            console.log(" users reponses are : " + JSON.stringify($scope.listOfQuestion));
            $scope.submitDisabled = true;

            // inputs , regPageOid , responseOid
            // console.log("$scope.imgURI " + $scope.imgURI)
            // ading time stamp of issue : 
            var timeObj = {};
            timeObj.fieldName = "Timestamp";
            timeObj.filedInputValue = new Date();
            timeObj.fieldType = "Text";

            var statusField = {};
            statusField.fieldName = "Status";
            statusField.filedInputValue = "Open";
            statusField.fieldType = "Options";

            if (reponseOid == "" || reponseOid == undefined || reponseOid == null) {

                $scope.listOfQuestion.userinputs.push(timeObj)
                    // $scope.listOfQuestion.userinputs.push(statusField)
                console.log(" adding forced stuff ")

            }



            if ($scope.imgURI == "" || $scope.imgURI == undefined) {
                $ionicLoading.show({
                    template: 'Updating issue ...'
                });
                var ajaxObj = appAdminService.submitRegistrationResponse($scope.listOfQuestion, $scope.listOfQuestion.pageOid, reponseOid);
                handleAjaxPromiseResp(ajaxObj);
                return;
            }


            uploadFile($scope.imgURI);

            /*var ajaxObj = appAdminService.submitRegistrationResponse($scope.listOfQuestion, $scope.listOfQuestion.pageOid, reponseOid);
            handleAjaxPromiseResp(ajaxObj);*/

        }



        function handleAjaxPromiseResp(ajaxPromiseObj) {

            ajaxPromiseObj.then(function successCallback(response) {
                console.log("Success with response : " + JSON.stringify(response.data));

                if (response.data == "error") {
                    alert(" Sorry some internal error has occured. ");
                    statsUpdateService.log("Internal Error from function hangleAjaxPromiseResp" + " from page : " + appPage);
                    return;
                }
                pageDetailsId = response.data;
                $ionicLoading.hide();
                window.alert("Your issue has been successfully submitted");

            }, function errorCallback(response) {

                console.log(" response : " + JSON.stringify(response));
                statsUpdateService.log("user callback as failed during saveTab TextSubTab : with error as :  " + JSON.stringify(response))

            });

        }


        $timeout(function() {
            if (alertPopupV != undefined) {
                alertPopupV.close(); //close the popup after 3 seconds for some reason
            }

        }, 3000);


        function loadRegistrationQuestions() {
            var ret;
            console.log(" about to loadRegistrationQuestions with reponseOid " + reponseOid)
            if (reponseOid == "" || reponseOid == undefined || reponseOid == null) {
                console.log("execing : ret = appAdminService.loadPageDetailsV2(pageIntroOid);")
                ret = appAdminService.loadPageDetailsV2(pageIntroOid);
                handleQuestionsLoad(ret)
            } else {
                console.log("execing : ret = appAdminService.getIntro(reponseOid);")
                ret = appAdminService.getIntro(reponseOid);
                handleResponseLoad(ret)
            }


            // handleFullDetailsLoad(ret);
        }

        function handleQuestionsLoad(ret) {

            ret.then(function successCallback(response) {
                console.log("Success with response : " + JSON.stringify(response.data));
                // console.log("Success with response : " + response.data);
                populateQuestionsHTML(response)

                var isDiffFromCache = isDataDiffFromCache("appAdminService.loadPageDetailsV2" + JSON.stringify(pageIntroOid), response);
                console.log(" isDiffFromCache " + isDiffFromCache)

                if (isDiffFromCache) {

                    /*alertPopupV = $ionicPopup.alert({
                        title: 'Updated data',
                        template: 'There was change in this page, and it has been updated now.'
                    });*/


                    populateQuestionsHTML(response)
                }



            }, function errorCallback(response) {
                console.log(" response : " + JSON.stringify(response));
                alert(" Sorry , unable to submit your response ");
                return pageData;
            });
        }



        function populateQuestionsHTML(response) {
            if (response.data == "error") {
                alert(" Sorry some internal error has occured. ")
                return;
            }
            $scope.isStatusInQuestion = true
            $scope.listOfQuestion = response.data;
            for (var i = 0; i < $scope.listOfQuestion.userinputs.length; i++) {
                if ($scope.listOfQuestion.userinputs[i].fieldName == "Status") {
                    $scope.listOfQuestion.userinputs[i].defaultSelected = "Open";
                    $scope.listOfQuestion.userinputs[i].filedInputValue = "Open";
                }
            };
            console.log("modified :  response : " + JSON.stringify(response.data));
        }

        function handleResponseLoad(ret) {

            ret.then(function successCallback(response) {
                populateResponseHTML(response);

                var isDiffFromCache = isDataDiffFromCache("appAdminService.getIntro" + JSON.stringify(reponseOid), response);
                console.log(" isDiffFromCache " + isDiffFromCache)
                if (isDiffFromCache) {

/*                    alertPopupV = $ionicPopup.alert({
                        title: 'Updated data',
                        template: 'There was change in this page, and it has been updated now.'
                    });
*/

                    populateResponseHTML(response);
                }


            }, function errorCallback(response) {
                console.log(" response : " + JSON.stringify(response));
                alert(" Sorry , unable to submit your response ");
                // return pageData;
            });
        }

        function populateResponseHTML(response) {
            console.log(" in populating respons e: ")
            $scope.isStatusInQuestion = false;
            console.log("Success with response : " + JSON.stringify(response.data));
            console.log("Success with response : " + response.data);
            if (response.data == "error") {
                alert(" Sorry some internal error has occured. ")
                return;
            }


console.log(" response.data.values.length "+response.data.values.length+  " +response.data.values.userinputs "+response.data.values.userinputs.length)

for (var i = 0; i < response.data.values.userinputs.length; i++) {
    var userinputs = response.data.values.userinputs[i];
    if (userinputs.fieldName == "Timestamp") {

var dt = new Date(response.data.values.userinputs[i].filedInputValue);

var dtStr = dt.getDate()+"/"+(dt.getMonth()+1)+"/"+dt.getFullYear()+" "+dt.getHours()+":"+dt.getMinutes();

    // response.data.values.userinputs[i].filedInputValue = userinputs.filedInputValue.substring(0,10)+" "+userinputs.filedInputValue.substring(11,16)
    response.data.values.userinputs[i].filedInputValue = dtStr;

    }

}

            $scope.listOfQuestion = response.data.values;

            // console.log("  data to be populated : " + JSON.stringify($scope.listOfQuestion))
            reponseOid = response.data._id;
            console.log("pageDetailsId" + pageDetailsId);


        }



        $scope.changedDropDown = function(index) {
            console.log(" index " + index)
            var selectedObj = $scope.listOfQuestion.userinputs[index];
            console.log(" drop down selected : " + JSON.stringify(selectedObj))
            console.log(" drop input value :  selected : " + JSON.stringify(selectedObj.filedInputValue))



            for (var i = 0; i < $scope.listOfQuestion.userinputs.length; i++) {

                if ($scope.listOfQuestion.userinputs[i].fieldName == "Sub-Department") {
                    updateSubDept(selectedObj.filedInputValue, i);
                }
            }


        }

        $scope.uploadPicture = function() {

            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL,
                // destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions
                    // saveToPhotoAlbum: true
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.imgURI = "data:image/jpeg;base64," + imageData;
                //$scope.citem.filedInputValue = "data:image/jpeg;base64," + imageData;
                console.log(" imageData could be printed here " + $scope.imgURI.length)
                    // uploadFile("data:image/jpeg;base64," + imageData);


            }, function(err) {
                console.log("An error Occured" + JSON.stringify(err));
            });


        }

        $scope.takePicture = function() {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL,
                // destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions
                    // saveToPhotoAlbum: true
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.imgURI = "data:image/jpeg;base64," + imageData;
                //$scope.citem.filedInputValue = "data:image/jpeg;base64," + imageData;
                console.log(" imageData could be printed here " + $scope.imgURI.length)
                    // uploadFile("data:image/jpeg;base64," + imageData);


            }, function(err) {
                console.log("An error Occured" + JSON.stringify(err));
            });
        }



        function uploadFile(imageData) {

            var file_tobe_uploaded = getFile(imageData);
            if (file_tobe_uploaded == null) {
                return;
            }
            var i = 0;


            var params = {};
            params.userId = userINTROOID;
            params.value2 = pageIntroOid;
            params.index = i;



            var fd = new FormData();
            fd.append('picture', file_tobe_uploaded);
            fd.append('userId', userINTROOID);
            fd.append('value2', pageIntroOid);
            fd.append('index', i);




            $http.post(imageUploadURL, fd, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                })
                .success(function(data) {
                    // alert("  file updaloeded " + JSON.stringify(data))
                    win(data);
                })
                .error(function(data) {

                    console.log("  file updaloeded " + JSON.stringify(data))
                });


        }

        var win = function(r) {
            // console.log("Code = " + r.responseCode);
            // console.log("Response = " + r.response);
            // console.log("Sent = " + r.bytesSent);

            if (r != "error") {
                // $scope.
                // console.log(" r 2017" + r.response)
                var json = r;
                console.log(" r index " + json.index);
                var newimageUrl = "/user/images/" + json.fileName;
                console.log(" new fileName url  " + newimageUrl);
                $scope.newimageUrl = newimageUrl;


                var userinputs = {
                    fieldName: "Issue picture",
                    filedInputValue: newimageUrl
                };

                try {
                    // alert(" $scope.listOfQuestion.userinputs length:>" + $scope.listOfQuestion.userinputs.length);


                    $scope.listOfQuestion.userinputs.splice(7, 0, userinputs);


                    $scope.listOfQuestion.userinputs[7].filedInputValue = newimageUrl;
                    $scope.listOfQuestion.userinputs[7].fieldName = "Issue picture";

                } catch (err) {
                    alert(" err " + err)
                }

                // alert("  $scope.listOfQuestion.userinputs[7].filedInputValue  " + centralServer+ $scope.listOfQuestion.userinputs[7].filedInputValue)

                var timeObj = {};
                timeObj.fieldName = "Timestamp";
                timeObj.filedInputValue = new Date();
                timeObj.fieldType = "Text";

                // alert(" timeObj " + JSON.stringify(timeObj))

                var statusField = {};
                statusField.fieldName = "Status";
                statusField.filedInputValue = "Open";
                statusField.fieldType = "Options";

                // alert(" statusField " + JSON.stringify(statusField))

                if (reponseOid == "" || reponseOid == undefined || reponseOid == null) {
                    $scope.listOfQuestion.userinputs.push(timeObj)
                        // $scope.listOfQuestion.userinputs.push(statusField)
                        // alert(" pushed time ")
                }
                $ionicLoading.show({
                    template: 'Updating issue ...'
                });


                // alert("  about to submit req ");
                var ajaxObj = appAdminService.submitRegistrationResponse($scope.listOfQuestion, $scope.listOfQuestion.pageOid, reponseOid);
                handleAjaxPromiseResp(ajaxObj);
                /*                var ajaxObj = appAdminService.submitRegistrationResponse($scope.listOfQuestion, pageDetailsId);
                                handleAjaxPromiseResp(ajaxObj);*/
                var totalCount = 0;
            } else {
                alert(" Failed to upload " + JSON.stringify(r) + " Please try again after some time ");
            }
        }


        function updateSubDept(deptName, subDepIndex) {
            console.log(" deptName " + deptName + "  $scope.listOfQuestion.userinputs[subDepIndex] " + JSON.stringify($scope.listOfQuestion.userinputs[subDepIndex]))

            $scope.listOfQuestion.userinputs[subDepIndex].fieldType = 'Options';

            if (deptName == "Electrical") {
                $scope.listOfQuestion.userinputs[subDepIndex].fieldAdditionalInfo = [{
                    "optName": "No specific"
                }, {
                    "optName": "Siganges"
                }, {
                    "optName": "Lift/Escalators"
                }, {
                    "optName": "HVSE/BMS"
                }, {
                    "optName": "Fire Alarm System"
                }, {
                    "optName": "Fire Fighting System"
                }, {
                    "optName": "Water treatement"
                }, {
                    "optName": "Sevage treatment"
                }, {
                    "optName": "Solar Power"
                }, {
                    "optName": "Power Supply System"
                }, {
                    "optName": "PBB/Aerobridges"
                }, {
                    "optName": "Conveyer Belt"
                }]
            }

            if (deptName == "Civil") {
                $scope.listOfQuestion.userinputs[subDepIndex].fieldAdditionalInfo = [{
                    "optName": "No specific"
                }]
            }

            if (deptName == "Operations") {
                $scope.listOfQuestion.userinputs[subDepIndex].fieldAdditionalInfo = [{
                    "optName": "No specific"
                }, {
                    "optName": "Washrooms"
                }, {
                    "optName": "Sitting Area"
                }, {
                    "optName": "Housekeeping"
                }, {
                    "optName": "Queue (Check-in/Security)"
                }, {
                    "optName": "Lift/Escalators"
                }, {
                    "optName": "Queue _ Immigration"
                }, {
                    "optName": "Delay @ Customs"
                }]
            }

            if (deptName == "Facilities") {
                $scope.listOfQuestion.userinputs[subDepIndex].fieldAdditionalInfo = [{
                    "optName": "No specific"
                }, {
                    "optName": "Eating Facilities"
                }, {
                    "optName": "Shopping Facilities"
                }, {
                    "optName": "Trolley"
                }, {
                    "optName": "Car Park"
                }, {
                    "optName": "Drinking Water"
                }, {
                    "optName": "Washrooms"
                }, {
                    "optName": "AC"
                }, {
                    "optName": "ATM"
                }, {
                    "optName": "Pay Phones"
                }, {
                    "optName": "Money Exchange"
                }, {
                    "optName": "Lounges"
                }, {
                    "optName": "Taxi"
                }, {
                    "optName": "Others (Mention)"
                }]
            }

            if (deptName == "Airport System") {
                $scope.listOfQuestion.userinputs[subDepIndex].fieldAdditionalInfo = [{
                    "optName": "No specific"
                }, {
                    "optName": "Security"
                }, {
                    "optName": "FIDS"
                }, {
                    "optName": "CCTV"
                }, {
                    "optName": "PA System"
                }, {
                    "optName": "SITA"
                }, {
                    "optName": "IT"
                }, {
                    "optName": "Wifi"
                }]
            }



        }



    }
]);
