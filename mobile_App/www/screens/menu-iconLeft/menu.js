'use strict';
angular.module('myApp.menu-iconLeft', ['ngRoute'])
    // .config(['$routeProvider', function($routeProvider) {
    //     $routeProvider.when('/menu-iconLeft', {
    //         templateUrl: 'screens/menu-iconLeft/menu.html',
    //         controller: 'menuCtrl'
    //     });
    // }])
    .controller(
        'menuCtrl', [
            '$scope',
            '$state',
            '$location',
            '$http',
            '$routeParams',
            '$stateParams',

            '$interval',
            'appAdminService',
            'statsUpdateService',
            function($scope, $state, $location, $http, $routeParams, $stateParams, $interval, appAdminService, statsUpdateService) {
                console.log(" in Home page");
                var appPage = "viewPage";

                var pageSpecificData = {};
                pageSpecificData.centralServer = centralServer;
                pageSpecificData.showSplash = true;
                $scope.pageSpecificData = pageSpecificData;

                var categories = [];
                var citem = {};
                citem.subPageName = "";
                citem.subPageType = "";
                citem.pageOid = "";
                $scope.showPagData = {
                    appName: "",
                    description: "",
                    themeName: ""
                };


                categories.push(citem);
                //                        $scope.categories = categories;
                var pageDataToserver = {},
                    data = {};
                var pageIntroOid = globalPageIntroOid;

                // var getFullDetails = centralServer + '/getIntro';

                console.log(" pageIntroOid " + pageIntroOid);
                loadPageDetails(pageIntroOid);

                function loadPageDetails(pageIntroOid) {

                    var subPageJSOn = {};
                    // console.log("Trying my best to retrieve values for menu's sake dude !!!!");
                    statsUpdateService.log("came To ViewPage with pageIntroOid as --- " + pageIntroOid);
                    subPageJSOn.introOID = pageIntroOid;
                    // console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));
                    var serverDatsReplica = { "pageOid": "#12:103", "menubarLocation": "left", "appName": "Chandigarh Aitport Maintenance", "_type": "vertex", "description": "Airport App FInal 103(Live app)", "loginTemplate": "buttons", "logoImage": "/user/images/logo-13_0-12_103multipartBody6346696141569230611asTemporaryFile", "templateName": "None", "menubarTemplate": "iconLeft", "homeTemplate": "rowListFull", "categories": [{ "subPageName": "New Issue", "subPageType": "registration", "pageOid": "#47:0", "subPageTemplate": "WBG" }, { "pageOid": "#15:577", "subPageTemplate": "thumbSquareLeftWBG", "subPageName": "Existing Issues", "subPageType": "list" }], "_id": "#12:103", "category": "Lifestyle" };
                    pageDataToserver = serverDatsReplica;
                    $scope.showPagData.appName = pageDataToserver.appName;
                    $scope.showPagData.themeName = pageDataToserver.themeName;
                    $scope.pageSpecificData.imageSrc = 'img/left.png'
                    $scope.showPagData.description = pageDataToserver.description;
                    $scope.categories = pageDataToserver.categories;

                }



                $scope.home = function() {
                    console.log(" clicked goToHome ");
                    statsUpdateService.log("Going to Home from page : " + appPage);
                    $location.path('/home');
                };


                $scope.back = function() {
                    console.log(" clicked finish ");
                    currentApp = null;
                    statsUpdateService.log("Going to prev page from Page : " + appPage);
                    window.history.back();

                    //                    $location.path('/home');
                };


                $scope.viewSubPage = function(index, filterCriterea) {
                    console.log(" to be taken to based on " + JSON.stringify($scope.categories[index]));

                    statsUpdateService.log("Viewing subpage from function :  viewSubPage from file : " + appPage);

                    var subPageType = $scope.categories[index].subPageType;


                    var templateName = $scope.categories[index].subPageTemplate;
                    var redirectorName = subPageType.trim().toLowerCase() + "-" + templateName.trim();
                    var goToLink = "app." + redirectorName;

                    // var goToLink = subPageType.trim().toLowerCase() + "SubPageView";
                    // var goToLink = "app." + subPageType.trim().toLowerCase();

                    if (subPageType == "" || subPageType == null || subPageType == undefined) {
                        alert(" Tab not yet completely designed.");
                        return;
                    }

                    var dumyPoid = $scope.categories[index].pageOid;
                    if (dumyPoid != null) {
                        // goToLink = goToLink + '/' + escape(dumyPoid);
                        console.log("new to link app." + goToLink + dumyPoid)

                        currentPageDetails.filterCriterea = filterCriterea;

                        $state.go(goToLink, {
                            pageOid: dumyPoid,
                            filterCriterea: filterCriterea
                        }, { reload: true });


                    } else {
                        //                        goToLink = goToLink + '/notYetSaved';
                        alert("Either page did not get saved properly or Tab no longer exists.");
                        statsUpdateService.log("Either page did not get saved properly or Tab no longer exists from page : " + appPage);
                    }
                    // console.log(" goToLink :  " + goToLink);
                    // $location.path(goToLink);


                };





            }
        ]);
