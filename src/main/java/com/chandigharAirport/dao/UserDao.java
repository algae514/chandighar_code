package com.chandigharAirport.dao;

import java.util.List;

import com.chandigharAirport.web.model.User;
 
 
public interface UserDao {
 
    User findById(int id);
 
    void saveUser(User User);
     
    void deleteUserByUsername(String ssn);
     
    List<User> findAllUsers();
 
    User findUserByUsername(String ssn);
    
    void updateUser(User User);

	List<User> searchUsers(User user);

	List<User> searchUsersByUsername(String user);

	User searchUsersAuth(String username, String password);

	
 
}