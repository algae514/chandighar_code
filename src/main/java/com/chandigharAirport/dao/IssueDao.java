package com.chandigharAirport.dao;

import java.util.List;

import com.chandigharAirport.web.model.Issue;
import com.chandigharAirport.web.model.SearchCriteria;
 
 
public interface IssueDao {
 
    Issue findById(int id);
 
    void saveIssue(Issue Issue);
     
    void deleteIssueById(int ssn);
     
    List<Issue> findAllIssues();
    
    void updateIssue(Issue issue);

    List<Issue> findIssue(Issue issue);

	List<Issue> findIssue(SearchCriteria searchCriterea);

 
}